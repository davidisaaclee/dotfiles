function! DL_Log(message)
	echom a:message
endfunction

call DL_Log("$VIM=" . $VIM)
call DL_Log("$VIMRUNTIME=" . $VIMRUNTIME)

function! DL_LoadConfig(path)
	call DL_Log("Loading config: " . a:path)
	exec "source" . a:path
endfunction

function! DL_SourcePattern(config_pattern)
	for config_path in glob(a:config_pattern, v:false, v:true)
		call DL_LoadConfig(config_path)
		" exec "source " . config_path
	endfor
endfunction

call DL_SourcePattern($HOME . "/.vim/user/*.vim")

colorscheme PaperColor
set background=light

call DL_Log("Finished loading vimrc")
