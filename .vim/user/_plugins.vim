" install vim-plug if not already installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" register plugins
call plug#begin('~/.vim/plugged')

call DL_SourcePattern($HOME . "/.vim/user/plugins/*.vim")

call plug#end()
