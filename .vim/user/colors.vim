" nnoremap <Leader>cd :set background=dark<CR>
" nnoremap <Leader>cl :set background=light<CR>

set termguicolors

let g:PaperColor_Theme_Options = {
  \   'theme': {
  \     'default.light': {
  \       'override' : {
  \         'search_bg' : ['#e2e2e2', '7'],
  \         'search_fg' : ['NONE', '7'],
  \         'cursor_bg' : ['#e9c9f2', '24'],
  \         'cursor_fg' : ['NONE', '255']
  \       }
  \     },
  \     'default.dark': {
  \       'override' : {
  \         'search_bg' : ['#000000', '7'],
  \         'search_fg' : ['NONE', '7'],
  \         'cursor_bg' : ['#e9c9f2', '24'],
  \         'cursor_fg' : ['NONE', '255']
  \       }
  \     }
  \   }
  \ }
color PaperColor

" if dark mode is on, use background=dark
if systemlist("defaults read -g AppleInterfaceStyle")[0] == "Dark"
  set background=dark
else
  set background=light
endif

" autosave delay, cursorhold trigger, default: 4000ms
setl updatetime=0

" highlight the word under cursor (CursorMoved is inperformant)
autocmd CursorHold * call HighlightCursorWord()
function! HighlightCursorWord()
    " if hlsearch is active, don't overwrite it!
    let search = getreg('/')
    let cword = expand('<cword>')
    if match(cword, search) == -1
        exe printf('match Search /\V\<%s\>/', escape(cword, '/\'))
    endif
endfunction
