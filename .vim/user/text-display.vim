syntax on

" Use different carets for different modes (iTerm2 only)
" let &t_SI = "\<Esc>]50;CursorShape=1\x7"
" let &t_SR = "\<Esc>]50;CursorShape=2\x7"
" let &t_EI = "\<Esc>]50;CursorShape=0\x7" 

set number
set relativenumber

set cursorline

set scrolloff=5
