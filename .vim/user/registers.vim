" Put current file path relative to pwd into " register
noremap <Leader>rp :let @" = @%<CR>

" Put current file name into " register
noremap <Leader>rP :let @" = expand('%:t')<CR>

" Put " register into + register
noremap <Leader>rr :let @+ = @"<CR>
