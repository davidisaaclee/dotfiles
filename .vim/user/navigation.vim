
nnoremap <Leader>db :Denite -start-filter buffer<CR>
nnoremap <Leader>df :Denite -start-filter file/rec<CR>
nnoremap <Leader>d" :Denite -start-filter register<CR>
nnoremap <Leader>dg :Denite -start-filter grep:::!<CR>

nnoremap <Leader>u :e %:p:h<CR>
nnoremap <Leader>U :vs %:p:h<CR>
nnoremap - :e %:p:h<CR>

nnoremap <Leader>w <C-w>

call denite#custom#var('file/rec', 'command',
    \ ['rg', '--files', '--vimgrep'])

call denite#custom#var('grep', {
    \ 'command': ['rg', '--threads', '1'],
    \ 'recursive_opts': [],
    \ 'final_opts': [],
    \ 'separator': ['--'],
    \ 'default_opts': ['-i', '--vimgrep', '--no-heading'],
    \ })

autocmd FileType denite-filter call s:denite_filter_my_settings()
function! s:denite_filter_my_settings() abort
  imap <silent><buffer> <Esc>       <Plug>(denite_filter_quit) 
endfunction

au FileType denite call s:denite_mappings()

function! s:denite_mappings() abort
  nnoremap <silent><buffer><expr> <CR>
  \ denite#do_map('do_action')
  nnoremap <silent><buffer><expr> <Tab>
  \ denite#do_map('choose_action')
  nnoremap <silent><buffer><expr> d
  \ denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr> p
  \ denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> v
  \ denite#do_map('do_action', 'vsplitswitch')
  nnoremap <silent><buffer><expr> t
  \ denite#do_map('do_action', 'tabswitch')
  nnoremap <silent><buffer><expr> q
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> i
  \ denite#do_map('open_filter_buffer')
  nnoremap <silent><buffer><expr> <Space>
  \ denite#do_map('toggle_select').'j'
endfunction


" " Use ripgrep for file/rec Denite source
" call denite#custom#var('file/rec', 'command',
" \ ['rg', '--files', '--glob', '!.git'])

" call denite#custom#map(
" \ 'insert',
" \ '<C-j>',
" \ '<denite:move_to_next_line>',
" \ 'noremap'
" \)
" call denite#custom#map(
" \ 'insert',
" \ '<C-k>',
" \ '<denite:move_to_previous_line>',
" \ 'noremap'
" \)
" call denite#custom#map(
" \ 'insert',
" \ '<C-t>',
" \ '<denite:do_action:tabopen>',
" \ 'noremap'
" \)
" call denite#custom#map(
" \ 'insert',
" \ '<C-v>',
" \ '<denite:do_action:vsplit>',
" \ 'noremap'
" \)
" call denite#custom#map(
" \ 'insert',
" \ '<C-p>',
" \ '<denite:do_action:preview>',
" \ 'noremap'
" \)
" call denite#custom#map(
" \ 'insert',
" \ '<C-s>',
" \ '<denite:do_action:split>',
" \ 'noremap'
" \)
" call denite#custom#map(
" \ 'normal',
" \ 'v',
" \ '<denite:do_action:vsplit>',
" \ 'noremap'
" \)
" call denite#custom#map(
" \ 'normal',
" \ 's',
" \ '<denite:do_action:split>',
" \ 'noremap'
" \)
