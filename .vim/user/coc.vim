nmap <silent> gd <Plug>(coc-definition)
nmap <silent> <C-k> <Plug>(coc-diagnostic-prev)
nmap <silent> <C-j> <Plug>(coc-diagnostic-next)

""" coc do ___ """
" coc do lens
nmap <silent> <Leader>cdl <Plug>(coc-codelens-action)
" coc do fix
nmap <silent> <Leader>cdf <Plug>(coc-fix-current)
" coc do refactor
nmap <silent> <Leader>cdr <Plug>(coc-refactor)

""" coc go ___ """
" coc go type
nmap <silent> <Leader>cgt <Plug>(coc-type-definition)
" coc go defn
nmap <silent> <Leader>cgd <Plug>(coc-definition)
" coc go references
nmap <silent> <Leader>cgr <Plug>(coc-references)


""" Function text objects """
" in function
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
" a function
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <C-j> pumvisible() ? "\<Down>" : "\<C-j>"
inoremap <expr> <C-k> pumvisible() ? "\<Up>" : "\<C-k>"
" inoremap <expr> <Esc> pumvisible() ? "\<C-e>" : "\<Esc>"

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
nnoremap <silent> <Leader>h :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Alias Code Lens virtual text to Comment highlight group
highlight link CocCodeLens Comment
