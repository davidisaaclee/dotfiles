Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'Shougo/denite.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'w0rp/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'benmills/vimux'
Plug 'AndrewRadev/linediff.vim'
Plug 'jeetsukumaran/vim-indentwise'
Plug 'airblade/vim-gitgutter'
Plug 'vim-test/vim-test'

Plug 'zackhsi/sorbet-lsp'
