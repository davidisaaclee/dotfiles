" from :h 'tabstop'
" 	1. Always keep 'tabstop' at 8, set 'softtabstop' and 'shiftwidth' to 4
" 	   (or 3 or whatever you prefer) and use 'noexpandtab'.  Then Vim
" 	   will use a mix of tabs and spaces, but typing <Tab> and <BS> will
" 	   behave like a tab appears every 4 (or 3) characters.
set softtabstop=2
set shiftwidth=2
" set noexpandtab
set expandtab
