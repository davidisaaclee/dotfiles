" fugitive
noremap <Leader>gs :Gstatus<CR>:resize 15<CR>
noremap <Leader>gd :Gdiff<CR>
noremap <Leader>gD :vs<CR>:Git! diff<CR>
noremap <Leader>gb :Gblame<CR>

" gutter
let g:gitgutter_signs = 0
let g:gitgutter_highlight_linenrs = 1
set signcolumn=no

