" augroup setup-ale
"     autocmd!

"     autocmd FileType javascript,javascript.jsx,rust
"     \ nnoremap <buffer>gd :ALEGoToDefinition<CR>

"     autocmd FileType javascript,javascript.jsx,rust
"     \ set omnifunc=ale#completion#OmniFunc
" augroup END

let g:ale_linters.rust = ['rls', 'cargo']

" nnoremap <Leader>h :ALEHover<CR>
" let g:ale_completion_enabled = 1

nnoremap <C-j> :ALENext<CR>
nnoremap <C-k> :ALEPrevious<CR>
