" set all panes to equal sizing on terminal resize
au VimResized * wincmd =

function! DL_PushSurface()
	let l:offset = line2byte(line(".")) + col(".")
	let @c = win_getid() . ":" . bufnr("%") . ":" . l:offset
endfunction
" function! DL_PeekSurface()
" 	exec "buffer " . split(@c, ":")[1]
" endfunction
function! DL_SwapWithRegisteredSurface()
	let l:activeWindow = win_getid()
	let l:activeBuffer = bufnr("%")
	let l:activeOffset = line2byte(line(".")) + col(".")
	let l:registeredWindow = split(@c, ":")[0]
	let l:registeredBuffer = split(@c, ":")[1]
	let l:registeredOffset = split(@c, ":")[2]

	call win_gotoid(l:registeredWindow)
	exec l:activeBuffer . "buffer"
	exec "goto " . l:activeOffset

	call win_gotoid(l:activeWindow)
	exec l:registeredBuffer . "buffer"
	exec "goto " . l:registeredOffset

	call DL_PushSurface()
endfunction

nnoremap <Leader>m :call DL_PushSurface()<CR>
nnoremap <Leader>' :call DL_SwapWithRegisteredSurface()<CR>
