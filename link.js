const fs = require('fs');
const path = require('path');
const os = require('os');
const commandExists = require('command-exists').sync;

const args = process.argv.slice(2);

const targets = parseTargetConfigurations([
	{
		src: './vimrc',
		dst: path.join(os.homedir(), '.vimrc'),
	},
	{
		src: './.gvimrc',
		dst: path.join(os.homedir(), '.gvimrc'),
	},
	{
		src: './.vim',
		dst: path.join(os.homedir(), '.vim'),
		type: 'directory',
	},
	{
		src: './.chunkwmrc',
		dst: path.join(os.homedir(), '.chunkwmrc'),
		condition: () => commandExists('chunkwm'),
	},
	{
		src: './.skhdrc',
		dst: path.join(os.homedir(), '.skhdrc'),
		condition: () => commandExists('skhd'),
	},
]);

function parseTargetConfigurations(targets) {
	return targets.map(target => ({
		...target,
		condition: target.condition == null ? () => true : target.condition,
		type: target.type == null ? 'file' : target.type,
	}));
}

const preexistingTargets = targets.filter(t => fs.existsSync(t.dst));
if (preexistingTargets.length > 0) {
	if (!args.includes('-f') && !args.includes('--force')) {
		console.log("Some files would be overwritten by this command. Run with `--force` to overwrite.");
		preexistingTargets.forEach(target => console.log(`\t${target.dst}`));
		process.exit(0);
	}
}

for (const target of targets) {
	if (!target.condition()) {
		continue;
	}

	// not really
	try {
		const stats = fs.statSync(target.dst);
		if (stats.isSymbolicLink() || stats.isFile()) {
			fs.unlinkSync(target.dst);
		} else {
			fs.rmdirSync(target.dst);
		}
	} catch (e) {
		if (e.code !== 'ENOENT') {
			console.log(e);
			process.exit(e.errno);
		}
	}

	try {
		fs.symlinkSync(path.resolve(target.src), path.resolve(target.dst));
	} catch (e) {
		if (e.code !== 'EEXIST') {
			console.log(e);
			process.exit(e.errno);
		}
	}
}
